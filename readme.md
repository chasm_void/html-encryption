# Simple Javascript-driven HTML content encryption #

Obfuscate content in a semi-secure manner. Baffle search engines. Regain a modicum of control over your data.

## What it does ##

Selectively encrypt elements on an HTML page (ie. a personal web page) with a password. The data is stored entirely in the HTML, and client-side Javascript is used to decrypt the results.

 * A symmetric encryption/decryption key is generated from the password text using [PBKDF2](http://en.wikipedia.org/wiki/PBKDF2)
 * The selectively defined data is encrypted using [AES](http://en.wikipedia.org/wiki/Advanced_Encryption_Standard)

Please note that there are numerous issues. An incomplete list appears below.

## What it doesn't do ##

**** Please read this carefully. ****

This approach is not cryptographically secure. There are a number of weaknesses that remain unaddressed. These weaknesses include, but are not limited to:

 * Password weakness. The algorithm uses symmetric AES encryption, and it occurs on the client side. It may be possible to brute-force the result using a powerful machine and an educated guess as to what the password is.
 * Encrypting text that is available online (for example, a search-engine-cached version of your site prior to applying this encryption) may ease malicious decryption of your site's content.
 * The password is stored in plain text, on your content-encryption server (ie. your personal computer). If this is stolen or your system is compromised, anyone can easily read your site's content.
 * There is no reliable way to audit access. (You cannot know if an unauthorized person has viewed your site's encrypted content.)
 * Likewise, there is no reliable way to track or prevent proliferation of the decrypted data. A malicious user can decrypt and then post any and all materials from your site. An unsuspecting user may have a compromised system that leaks their browser's local cache file. Etc.
 * There are no restrictions to password proliferation, for example:
     * Scenario: You gave your password to a friend. Your friend gave your password to an untrusted party (or posted it online). Result: any number of people may be able to read your site's content.
     * Scenario: Your password was compromised in any of the above ways. You change the password, and regenerate the web page, deploying a newly-encrypted version to the server. However, search engines have since cached snapshots of your web page, which are easily available online. Result: anyone with the compromised password can read your site's data as of the latest search engine caching crawl.
 * **This list is surely incomplete as I am not a cryptography/security expert.**

## So why should I use it? ##

Given the multitude of weaknesses applicable to this encryption approach, why use it?

 * People casually surfing your web site will not be able to read your site's content
 * Search engines will be unable to correctly parse and index your page, as far as I can tell
 * Easily deployable to places where you cannot manage `.htaccess` files or otherwise set access passwords (eg. Neocities.org)
 * Does not rely on any cloud service. Does not rely on any third party whatsoever. You remain in control of your data.
 
In short: this is meant to provide a strong (but not impenetrable) curtain for your web presence. You can choose what portions of your web page are visible, and what need decryption. This choice, and your data, remain yours -- no corporation ever gains access.

This project is part of a movement whose intent is to increase and bolster safe spaces online.