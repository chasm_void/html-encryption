require 'yaml'
require 'aes'
require 'digest'
require 'nokogiri'
require 'openssl'

config = YAML.load_file('configuration.yml');

input_file = File.open(config["input_file_name"])
doc = Nokogiri::HTML(input_file)
input_file.close

#these three parameters are not secret and must match the corresponding Javascript
salt = config["password_salt"]
iterations = 20000;
key_len = 16;

key_plaintext = config["secret_key"];
key = OpenSSL::PKCS5.pbkdf2_hmac_sha1(key_plaintext, salt, iterations, key_len)

#unpack into hex string to match AES input
key = key.unpack('H*').first

elements = doc.css(config["target_element_selector"]);
elements.each {|element|
	html = element.inner_html.to_s
	
	iv = AES.iv(:base_64)
	encrypted = AES.encrypt(html, key, {iv: iv, cipher: "AES-128-CBC",});
	
	element.content = encrypted
}

output_file = File.open(config["output_file_name"], "w");
doc.write_to(output_file);
output_file.close;